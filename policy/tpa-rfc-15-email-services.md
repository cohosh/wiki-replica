---
title: TPA-RFC-15: email services
---

[[_TOC_]]

Summary: TODO.

# Background

In late 2021, the TPA team adopted the following first Objective and
Key Results (OKR):

Improve mail services:

 1. David doesn't complain about "mail getting into spam" anymore
 2. RT is not full of spam
 3. we can deliver and receive mail from state.gov

This seemingly simple objective actually involves major changes to the
way email is handled on the `torproject.org` domain. Specifically, we
believe we will need to implement standards like SPF, DKIM, and DMARC
to have our mail properly delivered to large email providers, on top
of keeping hostile parties from falsely impersonating us.

TODO: mention history, howto/submission#Discussion, etc

TODO: related [make a plan regarding mail standards (DKIM,SPF, DMARC)][]

[make a plan regarding mail standards (DKIM,SPF, DMARC)]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40363

## Known issues

TODO: expand on current problems.

 * [Yahoo](https://gitlab.torproject.org/tpo/tpa/team/-/issues/34134), [state.gov](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40202), [Gmail](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40170), [Gmail again](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40149) (from the roadmap item)
 * Civi lacking entries (tpo/web/donate-static#15)
 * complaints about lists.tpo lacking SPF/DKIM (https://gitlab.torproject.org/tpo/tpa/team/-/issues/40347)
 * DMARC and lists.tpo (https://gitlab.torproject.org/tpo/tpa/team/-/issues/19914)

interlocking issues:

 * [x] the submission mail server (https://gitlab.torproject.org/tpo/tpa/team/-/issues/30608) requires upgrades and changes to ud-ldap #40062 and #40182 
 * [ ] general SPF deployment requires the submission mail server
 * [ ] DKIM on lists.tpo (https://gitlab.torproject.org/tpo/tpa/team/-/issues/40347) requires DMARC workarounds (https://gitlab.torproject.org/tpo/tpa/team/-/issues/40347)
 * [ ] general DKIM deployment requires testing (e.g. on tpo/web/donate-static#15) and integration with DNS (how?)
 * [ ] general DKIM deployment requires the submission mail server as well
 * [ ] SPF and DKIM require DMARC to properly function
 * [ ] DMARC requires a monitoring system to be effectively enabled (otherwise you might break legitimate emails going out and never know about it)
 * [ ] in any case, we need end-to-end deliverability tests to see if measures we take have an impact, see tpo/tpa/team#40494

The current situation is:

 * no DKIM deployment, except bridgedb which *verifies* incoming DKIM signatures (*but* does nothing with it, actually)
 * no SPF deployment, except on lists.tpo and crm.tpo
 * no DMARC deployment or reporting
 * no end-to-end deliverability testing, other than CiviCRM and Mailman internal monitoring systems and word-of-mouth
 * most servers relay their mails through eugeni except bridges, gettor, GitLab, db.tpo (alberti), RT, submit-01 and CiviCRM (according to the profile::postfix Puppet class setting, which is also enabled by the profile::dovecot::private)
 * we run restricted or "private" Dovecot servers (restricted to a single user) on only two servers (GitLab, CiviCRM), and such a configuration could be used to receive DMARC reports, for example... we also run an authentication-only (no mailbox or IMAP) dovecot for SASL authentication in postfix (on `submit-01`)

# Proposal

## Scope

## Affected users

# Roadmap brainstorm

TODO: reshuffle this in a timeline

 1. start filtering incoming mail for SPF and DKIM DMARC (postfix policy daemon?)
 2. deliverability monitoring:
    a. publish DMARC record to get reports to estimate effects of adding SPF records
    b. e2e deliverability tests, might not work for all mailboxes
    c. monitor metrics offered by postmaster.google.com, and microsoft's and ...? https://gitlab.torproject.org/tpo/tpa/team/-/issues/40168
 3. DKIM on outgoing mail, globally 
    a. watch out for DKIM forgeries
    b. decide key rotation policy (how frequently, should we publish private keys https://blog.cryptographyengineering.com/2020/11/16/ok-google-please-publish-your-dkim-secret-keys/)
 4. everyone adopts the submission server
    a. aliases are removed or,
    b. transformed into LDAP accounts or,
    c. they can't use them for outgoing
    d. possible that we'll have to stop email forwarding altogether, to be investigated
 5. once submission is adopted, add SPF records, soft ~
 6. DMARC, SPF, hard -
 7. ARC? meh? worth looking into it.

## Caveats

lots of work on the puppet module to be done (aka replacing complex custom in-house code)

TODO: estimate how much this will cost, look at previous estimate in howto/submission

## Open questions

 * how much do we value running our own mail server? how much privacy
   gains do we have?

## Other ideas

TODO: make an explicit alternative proposal to go with a separate hoster

Alternative ideas:

 * look into how friends of Tor are hosting their mail (Tails?
   Guardian Project (self-hosted, gmail, mayfirst)? EFF (outlook)?
   Calyx (self-hosted)? etc)
 * consider hosting mail at a third party (riseup? fastmail? gmail?? outlook???)
 * consider outsourcing *parts* of it

TODOs:

 * evaluate greenhost? 
 * ask infrared
 * talk with riseup
 * talk with isa

# Examples

TODO: take a few personas and describe what will happen to them

Examples:

 * ...

Counter examples:

 * ...

# Deadline

TODO.

# Status

This proposal is currently in the `draft` state.

# References

TODO.
